import Provider from "./Provider";

const plantProvider = new Provider();

export const allPlants = (params) => {

  return plantProvider.createRequest({
    url: "https://6nrr6n9l50.execute-api.us-east-1.amazonaws.com/default/front-plantTest-service",
    method: "GET",
    params: params
  });
  
};

export const plantDetail = (params) => {

  return plantProvider.createRequest({
    url: "https://6nrr6n9l50.execute-api.us-east-1.amazonaws.com/default/front-plantTest-service/plant",
    method: "GET",
    params: params
  });

};
