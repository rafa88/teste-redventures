import Provider from "./Provider";

const contactProvider = new Provider();

export const sendMessage = (params) => {

  return contactProvider.createRequest({
    url: "https://app.greenhouse.io/tests/767fba4eaa9cbb1289c7877047ef847c",
    method: "POST",
    data: params
  });

};
