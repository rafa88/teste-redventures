import Axios from "axios";

export default class Provider {
  constructor(config) {
    this.config = config;
  }

  createRequest(config) {
    this.config = config
    return new ProviderRequest(config);
  }
}

class ProviderRequest {
  constructor(config) {
    this.config = config
  }
  async createRequest({ onError = {}, onSuccess = {} }) {
    const result = await Axios(this.config);

    if (
      result &&
      result.status &&
      result.status >= 200 &&
      result.status < 300
    ) {
      return onSuccess(result.data);
    } else {
      return onError(result.data);
    }
  }
}