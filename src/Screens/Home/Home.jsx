import React from "react";
import { Link } from "react-router-dom";

import Header from "../../Components/Header/Header";

import illustration from "../../images/illustrations/illustration-home.png";
import illustrationMobile from "../../images/illustrations/illustration-home-mobile.png";
import "../../styles/Home.scss";

function Home() {
  return (
    <div className="home bg-grey">
      <div className="home-container">
        <div className="home-content">
          <div className="home-content-start">
            <div className="home-content-start-text">
              <Header />
              <h1>
                Find your
                <br className="d-sm-none" /> next
                <br className="d-md-none" /> green
                <br className="d-sm-none" /> friend
              </h1>
              <Link to="/quiz" className="btn btn-green-full">
                <i className="btn-icon btn-icon-white-next"></i>
                start quizz
              </Link>
            </div>
          </div>
          <div className="home-content-illustration">
            <img
              src={illustration}
              srcSet={`${illustrationMobile} 768w, ${illustrationMobile} 990w, ${illustration} 1000w`}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
