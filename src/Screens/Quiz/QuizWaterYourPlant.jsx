import React from "react";
import { Link, withRouter } from "react-router-dom";
import wateringcan from "../../images/illustrations/wateringcan.png";

import "../../styles/Quiz.scss";
import AskComponent from "./AskComponent";
import Header from "../../Components/Header/Header";
import { URL_QUIZ } from "../../Routes";

class QuizWaterYourPlant extends React.Component {
  state = {
    params: null
  };

  onClickQuiz(value) {
    if (value === this.state.params) {
      this.setState({ params: null });
    } else {
      this.setState({ params: value });
    }
  }

  redirect = () => {
    const { match: { url } } = this.props;
    this.props.history.push(`${url}/${this.state.params}`);
  }

  render() {
    const { params } = this.state;
    const asks = [
      { text: "Rarely", image: "one-drop", param: "rarely" },
      { text: "Regularly", image: "two-drops", param: "regularly" },
      { text: "Daily", image: "three-drops", param: "daily" }
    ];

    return (
      <div className="container container-center bg-grey">
        <div className="centralized-box">
          <Header className="header-vertical" />
          <div className="content ask">
            <header className="ask-header">
              <img src={wateringcan} alt="watering can" />
              <h1>
                How often do you want to
                <br className="d-sm-none" /> <strong>water</strong> your plant?
              </h1>
            </header>
            <section className="ask-list flex">
              {asks.map(ask => (
                <div
                  className="ask-list--item"
                  key={ask.image}
                  onClick={e => this.onClickQuiz(ask.param)}
                >
                  <AskComponent
                    type="green"
                    text={ask.text}
                    image={ask.image}
                    active={ask.param === params ? true : false}
                  />
                </div>
              ))}
            </section>
            <section className="ask-pagination flex justify-content-space-between">
              <Link to={URL_QUIZ} className="btn btn-green-stroke">
                <i className="btn-icon btn-icon-green-prev"></i>
                previous
              </Link>
              <button
                className="btn btn-green-stroke"
                disabled={!params}
                onClick={this.redirect}
              >
                <i className="btn-icon btn-icon-green-next"></i>
                next
              </button>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(QuizWaterYourPlant);
