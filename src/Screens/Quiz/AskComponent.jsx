import React from "react";
import PropTypes from "prop-types";

import "../../styles/AskComponent.scss";

const AskComponent = ({text, image, active, type}) => {
  return (
    <div className={`box-ask flex align-items-center flex-direction-column box-ask--${type} ${active ? "box-ask--active" : ""}`}>
      <div className={`box-ask--image box-ask--image-${image}`}></div>
      <p>{text}</p>
    </div>
  );
};

AskComponent.propTypes = {
  text: PropTypes.string
};

export default AskComponent;
