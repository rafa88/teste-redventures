import React from "react";
import { Link, withRouter } from "react-router-dom";
import dog from "../../images/illustrations/dog.png";

import "../../styles/Quiz.scss";
import AskComponent from "./AskComponent";
import Header from "../../Components/Header/Header";
import { URL_QUIZ, URL_PRODUCTS } from "../../Routes";

class QuizHaveDogs extends React.Component {
  state = {
    params: null
  };

  onClickQuiz(value) {
    if (value === this.state.params) {
      this.setState({ params: null });
    } else {
      this.setState({ params: value });
    }
  }

  redirect = () => {
    const { match: { params } } = this.props;
    this.props.history.push(`${URL_PRODUCTS}/${params.sun}/${params.water}/${this.state.params}`);
  }

  render() {
    const { params } = this.state;
    const asks = [
      { text: "Yes", image: "pet", param: "true" },
      { text: "No/They don't care", image: "no-answer", param: "false" }
    ];

    return (
      <div className="container container-center bg-grey">
        <div className="centralized-box">
          <Header className="header-vertical" />
          <div className="content ask">
            <header className="ask-header">
              <img src={dog} alt="dog" />
              <h1>Do you have pets? Do they chew plants?</h1>
              <p>
                <small>
                  We are asking because some plants can be{" "}
                  <strong>toxic</strong> for your buddy.
                </small>
              </p>
            </header>
            <section className="ask-list ask--small flex">
              {asks.map(ask => (
                <div
                  className="ask-list--item"
                  key={ask.image}
                  onClick={e => this.onClickQuiz(ask.param)}
                >
                  <AskComponent
                    type="coral"
                    text={ask.text}
                    image={ask.image}
                    active={ask.param === params ? true : false}
                  />
                </div>
              ))}
            </section>
            <section className="ask-pagination ask--small flex justify-content-space-between">
              <Link
                to={`${URL_QUIZ}/${this.props.match.params.waterYourPlant}`}
                className="btn btn-green-stroke"
              >
                <i className="btn-icon btn-icon-green-prev"></i>
                previous
              </Link>
              <button
                className="btn btn-green-stroke"
                disabled={!params}
                onClick={this.redirect}
              >
                <i className="btn-icon btn-icon-green-next"></i>
                finish
              </button>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(QuizHaveDogs);
