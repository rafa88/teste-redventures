import React from "react";
import { Link, withRouter } from "react-router-dom";
import sun from "../../images/illustrations/sun.png";

import "../../styles/Quiz.scss";
import AskComponent from "./AskComponent";
import { URL_QUIZ, URL_HOME } from "../../Routes";
import Header from "../../Components/Header/Header";

class Quiz extends React.Component {
  state = {
    params: null
  };

  onClickQuiz(value) {
    if (value === this.state.params) {
      this.setState({ params: null });
    } else {
      this.setState({ params: value });
    }
  }

  redirect = () => {
    this.props.history.push(`${URL_QUIZ}/${this.state.params}`);
  }

  render() {
    const { params } = this.state;
    const asks = [
      { text: "High sunlight", image: "sun", param: "high" },
      { text: "Low sunlight", image: "low-sun", param: "low" },
      { text: "No sunlight", image: "no-answer", param: "no" }
    ];

    return (
      <div className="container container-center bg-grey">
        <div className="centralized-box">
          <Header className="header-vertical" />
          <div className="content ask">
            <header className="ask-header">
              <img src={sun} alt="Sun" />
              <h1>
                First, set the amount of
                <br className="d-md-none" />
                <br className="d-sm-none" /> <strong>sunlight</strong> your
                <br className="d-md-none" /> plant will get.
              </h1>
            </header>
            <section className="ask-list flex">
              {asks.map(ask => (
                <div
                  className="ask-list--item"
                  key={ask.image}
                  onClick={e => this.onClickQuiz(ask.param)}
                >
                  <AskComponent
                    type="coral"
                    text={ask.text}
                    image={ask.image}
                    active={ask.param === params ? true : false}
                  />
                </div>
              ))}
            </section>
            <section className="ask-pagination flex justify-content-space-between">
              <Link to={URL_HOME} className="btn btn-green-stroke">
                <i className="btn-icon btn-icon-green-prev"></i>
                home
              </Link>
              <button
                className="btn btn-green-stroke"
                disabled={!params}
                onClick={this.redirect}
              >
                <i className="btn-icon btn-icon-green-next"></i>
                next
              </button>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Quiz);
