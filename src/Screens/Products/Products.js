import React from "react";
import { withRouter } from "react-router-dom";
import imagesHeader from "../../images/illustrations/pick.png";

import "../../styles/Products.scss";
import Header from "../../Components/Header/Header";
import ProductItem from "./ProductItem";
import { allPlants } from "../../Services/PlantsServices";
import Spinner from "../../Components/Spinner/Spinner";

class Products extends React.Component {

  state = {
    plants: [],
    spinner: true
  }
  
  componentDidMount() {
    const plants = allPlants(this.props.match.params);

    plants.createRequest({
      onError: result => {
        console.log(result)
        this.setState({ spinner: false });
      },
      onSuccess: result => {
        this.setState({ plants: result, spinner: false });
      }
    });
  }
  
  render() {
    const { plants, spinner } = this.state

    return (
      <div className="container bg-grey">
        <Header className="header-vertical" />
        {spinner && <Spinner />}
        {!spinner &&
          <div className="content products">
            <header className="products-header">
              <img src={imagesHeader} alt="Dog" />
              <h1>Our picks for you</h1>
            </header>
            <div className="products-list">
              {plants.length > 0 && (
                <section className="products-list--scroll flex flex-wrap">
                  {plants.map(product => (
                    <div className="products-list--item" key={product.id}>
                      <ProductItem product={product} />
                    </div>
                  ))}
                </section>
              )}
            </div>
          </div>
        }
      </div>
    );
  }
}

export default withRouter(Products);
