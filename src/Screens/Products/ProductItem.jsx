import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import { URL_PRODUCTS } from "../../Routes";

import "../../styles/ProductItem.scss";

const ProductItem = ({ product }) => {
  return (
    <div className="product-item">
      <div className="product-item--image">
        <img src={product.url} alt="Dog" />
      </div>
      <div className="product-item--name">{product.name}</div>
      <div className="product-item--info flex align-items-center">
        <div className="product-item--info-price">
          <p>${product.price}</p>
        </div>
        <div className="product-item--info-icons flex justify-content-flex-end">
          {product.toxicity && <i className="icon toxic"></i>}
          {product.sun === "high" && <i className="icon high-sun"></i>}
          {product.sun === "low" && <i className="icon low-sun"></i>}
          {product.sun === "no" && <i className="icon no-answer"></i>}
          {product.water === "daily" && <i className="icon three-drops"></i>}
          {product.water === "regularly" && <i className="icon two-drops"></i>}
          {product.water === "rarely" && <i className="icon one-drop"></i>}
        </div>
      </div>
      <div className="product-item--button">
        <Link to={`${URL_PRODUCTS}/${product.id}`} className="btn btn-green-stroke">
          buy now
        </Link>
      </div>
    </div>
  );
};

ProductItem.propTypes = {
  product: PropTypes.object
};

export default ProductItem;
