import React from "react";
import { withRouter } from "react-router-dom";

import icons from "../../images/icons/sprites.svg";

import "../../styles/ProductsDetail.scss";
import Header from "../../Components/Header/Header";
import { plantDetail } from "../../Services/PlantsServices";
import ProductContact from "./ProductContact";
import Spinner from "../../Components/Spinner/Spinner";

class ProductsDetail extends React.Component {

  state = {
    plant: null,
    spinner: true
  }

  componentDidMount() {
    const request = plantDetail(this.props.match.params);

    request.createRequest({
      onError: error => {
        console.log(error)
        this.setState({ spinner: false });
      },
      onSuccess: result => {
        this.setState({ plant: result, spinner: false });
      }
    });
  }

  render() {
    const { plant, spinner } = this.state;
    const sun = { high: "High sunlight", low: "Low sunlight", no: "No sunlight" };
    const water = { rarely: "Water rarely", regularly: "Regularly", daily: "Daily" };

    return (
      <div className="container container-center">
        <div className="centralized-box">
          <Header className="header-vertical" />
          {spinner && <Spinner />}
          {(plant && !spinner) && (
            <div className="content product-detail">
              <div className="product-detail--content">
                <section className="plant">
                  <h1 className="plant-name">{plant.name}</h1>
                  <p className="plant-price">${plant.price}</p>
                  <div className="plant-image">
                    <img src={plant.url} alt={plant.name} />
                  </div>
                  <ul className="plant-info">
                    <li>
                      <svg className="icon-svg">
                        <use xlinkHref={`${icons}#grey-high-sun`}></use>
                      </svg>
                      {sun[plant.sun]}
                    </li>
                    <li>
                      {plant.water === "daily" && (
                        <svg className="icon-svg">
                          <use xlinkHref={`${icons}#grey-three-drops`}></use>
                        </svg>
                      )}
                      {plant.water === "regularly" && (
                        <svg className="icon-svg">
                          <use xlinkHref={`${icons}#grey-two-drops`}></use>
                        </svg>
                      )}
                      {plant.water === "rarely" && (
                        <svg className="icon-svg">
                          <use xlinkHref={`${icons}#grey-one-drop`}></use>
                        </svg>
                      )}
                      {water[plant.water]}
                    </li>
                    <li>
                      {plant.toxicity ? (
                        <svg className="icon-svg">
                          <use xlinkHref={`${icons}#grey-toxic`}></use>
                        </svg>
                      ) : (
                        <svg className="icon-svg">
                          <use xlinkHref={`${icons}#grey-pet`}></use>
                        </svg>
                      )}
                      {plant.toxicity ? "Toxic for pets" : "Non-toxic for pets"}
                    </li>
                  </ul>
                </section>
                <ProductContact />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withRouter(ProductsDetail);
