import React from "react";
import { withRouter } from "react-router-dom";

import image from "../../images/illustrations/envelop.png";

import "../../styles/ProductContact.scss";
import { sendMessage } from "../../Services/ContactServices";
import { validation } from "../../Healpers/Validation";

class ProductContact extends React.Component {

  state = {
    values: {name: '', email: ''},
    errors: {},
    sent: false
  }

  handleSubmit = (e) => {
    e.preventDefault();
    
    const errors = validation(this.state.values)
    this.setState({ errors });
  
    if (Object.keys(errors).length === 0) {
      const request = sendMessage({ ...this.state.values, id: this.props.match.id});
      request.createRequest({
        onError: error => {
          console.log(error)
        },
        onSuccess: result => {
          this.setState({ plant: result })
        }
      });
    }
  }

  handleInputChange = (e) => {
    this.setState({ values: Object.assign({}, this.state.values, {[e.target.id]: e.target.value}) });
  }

  render() {
    const {values, errors, sent} = this.state;
    return (
      <section className={`form bg-grey ${sent && "center"}`}>
        {!this.state.sent ? (
          <>
            <h2 className="form-title">Nice pick!</h2>
            <p className="form-subtitle">
              Tell us your name and e-mail and we will get in touch regarding
              your order ;)
            </p>
            <form onSubmit={this.handleSubmit}>
              <div className={`form-input ${errors.name ? "error" : ""}`}>
                <label>Name</label>
                <input
                  id="name"
                  value={values.name}
                  onChange={this.handleInputChange}
                  autoComplete="off"
                  placeholder="Crazy Plant Person"
                />
                <small className="message">{errors.name}</small>
              </div>
              <div className={`form-input ${errors.email ? "error" : ""}`}>
                <label>E-mail</label>
                <input
                  id="email"
                  value={values.email}
                  onChange={this.handleInputChange}
                  autoComplete="off"
                  placeholder="plantperson@email.com"
                />
                <small className="message">{errors.email}</small>
              </div>
              <div className="form-button">
                <button className="btn btn-sm-width btn-sm-green-full btn-green-stroke">
                  send
                </button>
              </div>
            </form>
          </>
        ) : (
          <>
            <h2 className="form-title">Thank you!</h2>
            <p className="form-subtitle">
              You will hear from us soon. Please check your e-mail!
            </p>
            <img className="form-image" src={image} />
          </>
        )}
      </section>
    );
  }
}

export default withRouter(ProductContact);
