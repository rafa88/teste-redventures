import React from "react";
import logo from "../../images/logo/logo-greenthumb.svg";

import "../../styles/Header.scss";

const Header = ({className}) => {
  return (
    <header className={`header ${className}`}>
      <img src={logo} alt="green thumb" />
    </header>
  );
};
export default Header;
