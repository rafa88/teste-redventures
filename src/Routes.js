import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Screens/Home/Home";
import Quiz from "./Screens/Quiz/Quiz";
import QuizWaterYourPlant from "./Screens/Quiz/QuizWaterYourPlant";
import QuizHaveDogs from "./Screens/Quiz/QuizHaveDogs";
import Products from "./Screens/Products/Products";
import ProductsDetail from "./Screens/Products/ProductsDetail";

export const URL_HOME = '/';
export const URL_QUIZ = "/quiz";
export const URL_QUIZ_WATER_YOUR_PLANT = "/quiz/:sun";
export const URL_HAVE_PETS = "/quiz/:sun/:water";
export const URL_PRODUCTS = "/products";
export const URL_PRODUCTS_LIST = `${URL_PRODUCTS}/:sun/:water/:pets`;
export const URL_PRODUCTS_DETAIL = `${URL_PRODUCTS}/:id`;

function Routes() {
  return (
    <Switch>
      <Route path={URL_HOME} exact={true} component={Home} />
      <Route path={URL_QUIZ} exact={true} component={Quiz} />
      <Route path={URL_QUIZ_WATER_YOUR_PLANT} exact={true} component={QuizWaterYourPlant} />
      <Route path={URL_HAVE_PETS} exact={true} component={QuizHaveDogs} />
      <Route path={URL_PRODUCTS_LIST} exact={true} component={Products} />
      <Route path={URL_PRODUCTS_DETAIL} exact={true} component={ProductsDetail} />
    </Switch>
  );
}

export default Routes;
